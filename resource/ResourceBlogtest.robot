*** Settings ***
Library  SeleniumLibrary
Library  FakerLibrary

*** Variables ***
${URL}  http://automationpractice.com/
${BROWSER}  chrome
${BTN_SING}  css=a.login
${INPUT_EMAIL}  css=input[id="email_create"]
${BUTTON_CREATE}  css=button[name="SubmitCreate"]
${SELECT_CHECKBOX}   css=input[id="id_gender2"]
${INPUT_NAME}   xpath=//input[@id="customer_firstname"]
${INPUT_SBNAME}  xpath=//input[@id="customer_lastname"]
${INPUT_PASSWORD}  xpath=//input[@id="passwd"]
${SELECT_DIA}  css=#days > option:nth-child(6)
${SELECT_MES}  css=#months > option:nth-child(13)
${SELECT_ANO}  css=#years > option:nth-child(33)
${INPUT_ENDEREÇO}  css=input#address1
${INPUT_CIDADE}  css=input[id="city"]
${SELECT_ESTADO}  css=#id_state > option:nth-child(6)
${INPUT_CEP}  css=input[id="postcode"]
${SELECT_PAÌS}  css=#id_country > option:nth-child(2)
${INPUT_CONTATO}  css=input[id="phone_mobile"]
${INPUT_ENDREFERENCE}  css=input[id="alias"]
${BTN_CONFIRMAR}  css=button[id="submitAccount"]

*** Keywords ***
Dado que acesso o site automationpractice
    Open Browser  url=${URL}  browser=${BROWSER}
    Maximize Browser Window

E que acesse a pagina de cadastro para criar uma nova conta informando um novo email
    ${EMAILFAKE}  FakerLibrary.Email
    Click Link  ${BTN_SING}
    Wait Until Element Is Visible  ${INPUT_EMAIL}
    Input Text  ${INPUT_EMAIL}  ${EMAILFAKE}
    Click Button  ${BUTTON_CREATE}

E preencho as informações pessoais de cadastro passando o nome, sobrenome e senha
    ${NAMEFAKE}  FakerLibrary.Name
    ${SOBRENOMEFAKE}  FakerLibrary.Name
    ${SENHAFAKE}  FakerLibrary.Password

    Wait Until Element Is Visible  ${SELECT_CHECKBOX}
    Click Element  ${SELECT_CHECKBOX}
    Sleep  2s
    Input Text  ${INPUT_NAME}  ${NAMEFAKE}
    Sleep  2s
    Input Text  ${INPUT_SBNAME}  ${SOBRENOMEFAKE}
    Sleep  2s
    Input Password  ${INPUT_PASSWORD}  ${SENHAFAKE}
    Sleep  2s
    Click Element  ${SELECT_DIA}
    Click Element  ${SELECT_MES}
    Click Element  ${SELECT_ANO}
    Sleep  2s

E preencho as informações de endereço, cidade, cep, "${NUMEROTEL}", referência
    ${ENDEREÇOFAKE}  FakerLibrary.Address
    ${CIDADEFAKE}  FakerLibrary.City
    ${CEPFAKE}  FakerLibrary.Postalcode
    ${REFERENCIAFAKE}  FakerLibrary.Address
    ${SENHAFAKE}  FakerLibrary.Password

    Wait Until Element Is Visible  ${INPUT_ENDEREÇO}
    Input Text  ${INPUT_ENDEREÇO}  ${ENDEREÇOFAKE}
    Sleep  2s
    Input Text  ${INPUT_CIDADE}  ${CIDADEFAKE}
    Sleep  2s
    Click Element  ${SELECT_ESTADO}
    Sleep  2s
    Wait Until Element Is Visible  ${INPUT_CEP}
    Sleep  2s
    Input Text  ${INPUT_CEP}  ${CEPFAKE}
    Sleep  2s
    Click Element  ${SELECT_PAÌS}
    Input Text  ${INPUT_CONTATO}  ${NUMEROTEL}
    Sleep  2s
    Input Text  ${INPUT_ENDREFERENCE}  ${REFERENCIAFAKE}
    Sleep  2s
    Input Password  ${INPUT_PASSWORD}  ${SENHAFAKE}
    Sleep  3s

Quando eu confirmo o cadastro clicando no botão Confirmar
    Wait Until Element Is Visible  ${BTN_CONFIRMAR}
    Click Button  ${BTN_CONFIRMAR}
    Sleep  1s

Então irei confirir a mensagem "${MENSAGEMDESEJADA}"
    Page Should Contain  text=${MENSAGEMDESEJADA}
    Sleep  3s
    Close Browser
