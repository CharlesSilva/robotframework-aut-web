*** Settings ***
Resource  ../resource/ResourceBlogtest.robot

*** Test Cases ***
Caso de teste 01: Cadastra um usuario
   Dado que acesso o site automationpractice
   E que acesse a pagina de cadastro para criar uma nova conta informando um novo email
   E preencho as informações pessoais de cadastro passando o nome, sobrenome e senha
   E preencho as informações de endereço, cidade, cep, "12998231232", referência
   Quando eu confirmo o cadastro clicando no botão Confirmar
   Então irei confirir a mensagem "Welcome to your account"